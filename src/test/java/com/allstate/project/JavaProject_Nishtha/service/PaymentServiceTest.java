package com.allstate.project.JavaProject_Nishtha.service;

import com.allstate.project.JavaProject_Nishtha.dao.PaymentDAO;
import com.allstate.project.JavaProject_Nishtha.entities.Payment;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.Mockito.doReturn;
import static org.mockito.MockitoAnnotations.initMocks;

@SpringBootTest
public class PaymentServiceTest {
    private PaymentService paymentService;
    private Date date = new Date();
    private  Payment result;
    private List<Payment> resultList;

    @Mock
    private PaymentDAO paymentDaoMock;
    @BeforeEach
    void setUp() {
        PaymentDAO paymentDaoMock = Mockito.mock(PaymentDAO.class);
        result = new Payment(1232, date, "Credit", 25000, 123 );
        doReturn(1L).when(paymentDaoMock).rowCount();
        doReturn(result).when(paymentDaoMock).findById(1232);
        doReturn(resultList).when(paymentDaoMock).findByType("Credit");
        paymentService = new PaymentServiceImpl(paymentDaoMock);
    }

    @Test
    void rowCount_Success(){
        assertEquals(1L, paymentService.rowCount());
    }


    @Test
    void findById_Success(){

        assertEquals(result ,paymentService.findById(1232));
    }

    @Test
    void findByType_Success(){
        assertEquals(resultList ,paymentService.findByType("Credit"));
    }

    @Test
    void findById_ReturnNull(){
        assertEquals(null, paymentService.findById(0));

    }


    @Test
    void findByType_ReturnNull(){
        assertNull(paymentService.findByType(null));
    }
}
