package com.allstate.project.JavaProject_Nishtha.dao;

import com.allstate.project.JavaProject_Nishtha.entities.Payment;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.doReturn;
import static org.mockito.MockitoAnnotations.initMocks;

@SpringBootTest
public class PaymentDAOTest {
    @Autowired
    private PaymentDAO paymentdao;
    private Date date = new  Date();

    @Mock
    private PaymentDAO daoMock;



    @Test
     void findById_Success() {
        Payment payment = new Payment(1230,date, "Debit",5700, 123);

        // paymentdao.save(payment);

        assertEquals(payment.getId(), paymentdao.findById(1230).getId());
    }

    @Test
    void findByType_Success(){
        List<Payment> paymentList = paymentdao.findByType("Credit");
        assertNotNull(paymentList);
    }
    @Test
    void rowCount_success() {
        doReturn(1L).when(daoMock).rowCount();
        assertEquals(1L, daoMock.rowCount());
    }
}
