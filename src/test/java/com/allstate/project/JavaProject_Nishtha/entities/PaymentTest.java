package com.allstate.project.JavaProject_Nishtha.entities;


import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class PaymentTest {

    Payment payment = new Payment();
    Date date = new Date();

    @Test
    void getAmountTest()
    {
        payment.setAmount(3000089);
        assertEquals(3000089, payment.getAmount());

    }
    @Test
    void getIdTest(){
        payment.setId(1);
        assertEquals(1, payment.getId());
    }

    @Test
    void getCustIdTest(){
        payment.setCustId(101);
        assertEquals(101, payment.getCustId());
    }

    @Test
    void getTypeTest(){
        payment.setType("Debit");
        assertEquals("Debit", payment.getType());
    }

    @Test
    void getPaymentDateTest(){
        payment.setPaymentDate(date);
        assertEquals(date, payment.getPaymentDate());
    }

    @Test
    void ConstructorTest(){
        payment = new Payment(11, date, "Credit", 123456, 102);
        assertEquals(11, payment.getId());
        assertEquals(date, payment.getPaymentDate());
        assertEquals(123456, payment.getAmount());
        assertEquals(102, payment.getCustId());
        assertEquals("Credit", payment.getType());
    }

    @Test
    void toStringTest(){
        payment = new Payment(10, date, "Credit", 123456, 103);
        String toStringResult = payment.toString();
        assertEquals("123569.0Credit"+date, toStringResult);
    }
}
