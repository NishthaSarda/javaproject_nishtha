package com.allstate.project.JavaProject_Nishtha.controller;

import com.allstate.project.JavaProject_Nishtha.entities.Payment;
import com.allstate.project.JavaProject_Nishtha.service.PaymentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/payment")
public class PaymentController {
@Autowired
    private PaymentService paymentService;

    @RequestMapping( value = "/status", method = RequestMethod.GET)
    public String getStatus(){

        return "Payment Rest API Loaded Successfully";
    }

    @RequestMapping( value = "/count", method = RequestMethod.GET)
    public long getRowCount(){

        return paymentService.rowCount();
    }

    @RequestMapping( value = "/findById/{id}", method = RequestMethod.GET)
    public Payment findById(@PathVariable("id") int id){

        return paymentService.findById(id);
    }

    @RequestMapping( value = "/findByType/{type}", method = RequestMethod.GET)
    public List<Payment> findByType(@PathVariable("type") String type){

        return paymentService.findByType(type);
    }

    @RequestMapping( value = "/save", method = RequestMethod.POST )
    public void Save(@RequestBody Payment payment){
        paymentService.save( payment);
    }
}

