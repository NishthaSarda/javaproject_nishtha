package com.allstate.project.JavaProject_Nishtha.service;

import com.allstate.project.JavaProject_Nishtha.dao.PaymentDAO;
import com.allstate.project.JavaProject_Nishtha.entities.Payment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class PaymentServiceImpl  implements PaymentService{
    @Autowired
    PaymentDAO paymentDAO;

    public PaymentServiceImpl(PaymentDAO paymentDao) {
        this.paymentDAO = paymentDao;
    }
    public PaymentServiceImpl(){

    }

    @Override
    public long rowCount() {
        return paymentDAO.rowCount();
    }

    @Override
    public List<Payment> findByType(String type) {
        if(type!=null){
            return paymentDAO.findByType(type);
        }
        return null;
    }

    @Override
    public void save(Payment payment) {
        paymentDAO.save(payment);
    }

    @Override
    public Payment findById(int id) {
        if(id > 0){
            return paymentDAO.findById(id);
        }
        return null;
    }
}
