package com.allstate.project.JavaProject_Nishtha.service;

import com.allstate.project.JavaProject_Nishtha.entities.Payment;

import java.util.List;

public interface PaymentService {

    long rowCount();
    List<Payment> findByType(String type);
    void save(Payment payment);
    Payment findById(int id);
}
