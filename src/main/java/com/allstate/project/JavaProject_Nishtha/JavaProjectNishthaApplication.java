package com.allstate.project.JavaProject_Nishtha;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JavaProjectNishthaApplication {

	public static void main(String[] args) {

		SpringApplication.run(JavaProjectNishthaApplication.class, args);
	}

}
