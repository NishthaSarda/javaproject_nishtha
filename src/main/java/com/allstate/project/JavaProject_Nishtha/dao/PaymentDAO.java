package com.allstate.project.JavaProject_Nishtha.dao;

import com.allstate.project.JavaProject_Nishtha.entities.Payment;
import java.util.List;

public interface PaymentDAO {
    long rowCount();
    Payment findById(int id);
   // List<Payment> findAll();
    void save(Payment payment);
    List<Payment> findByType(String type);
}
