package com.allstate.project.JavaProject_Nishtha.dao;

import com.allstate.project.JavaProject_Nishtha.entities.Payment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public class PaymentDAOImpl implements PaymentDAO {
@Autowired
    MongoTemplate tpl;

    @Override
    public long rowCount() {
        Query query = new Query();
        return tpl.count(query, Payment.class);
    }

    @Override
    public Payment findById(int id) {
        Query query = new Query();
        query.addCriteria(Criteria.where("id").is(id));
        Payment payment  = tpl.findOne(query, Payment.class);
        return payment;
    }


    @Override
    public void save(Payment payment) {
        tpl.save(payment);
    }

    @Override
    public List<Payment> findByType(String type) {
        Query query = new Query();
        query.addCriteria(Criteria.where("type").is(type));
        List<Payment> paymentList  = tpl.find(query, Payment.class);
        return paymentList;
    }
}
